# Robocup Soccer Simulation League 2D #

## LARC/CBR2018 ##

### João Pessoa - PB, Brazil ###

This document contains the rules and procedures for the LARC/CBR2018 Soccer Simulation League 2D competition in João Pessoa – PB, Brazil

## Schedule

The detailed schedule will be determined at the competition site. [Click here.](http://www.cbrobotica.org/?page_id=28)

## Operating System

Linux Ubuntu LTS 16.04 64bit will be used. 

## Soccer Simulator

Current  version  of  RCSSServer  version  is  15.4.0. The  default  server  configuration  files  (server.conf  and player.conf)  generated  by  RCSSServer  15.4.0  will  be  used. [Click here](https://bitbucket.org/larc_cbr_2d_simulation/larc2017/wiki/Home) to get more information.

## General Tournament Rules

Note  that  the  number  of  turns, groups and teams depends of competitor’s number. 

* All matches will be started automatically by the league manager script. 
* Teams are only allowed to update their binaries by 1 hour before the first game starts in a day. Each team need to provide proper start, kill and team.yml scripts. Sample scripts will be in the top of your home directory (e.g. /home/helios/start).
* You MUST to provide binary name the same as team name. Binary names like sample_player won't be allowed. 
* In order to test the automatic running games script (the league manager), teams have to finish testing their binaries and scripts on a competition machine at the most 1 hour before competition being started.
* Script will be executed by a different user in your user group. Your scripts and your team have to be at least readable and be executable permission (chmod +x -R 755 *). 
* Double check that your kill scripts if them are killing all of your programs (goalie, players, and coach) even if your programs are finished automatically. If the scripts do not work properly, the organizers will not fix them. 
* Do NOT output too many data to stdout, stderr and files in your home directory because it might cause a serious network delay. 
* Do not change the team name during the tournament. The team name that is used in the first match of each  team must be  kept  unchanged.

Violating this rules may cause disqualification in the following LARC/CBR competitions. The home directory of each team will be published automatically after the competition.


### A. Tournament Score
In the group rounds, there will be 3 (three) points allocated for a win and 1 (one) point for a draw. After Group matches, playoff can be done in matches based on the results of the two-way aggregated matches or by one match followed two extra times. If at the final of round the scoreboard is the same, penalties will be executed automatically. The team that does not present the binary until the start of the round will not score points and the result of 3:0 will be awarded to the opposing team. 

### B. Tiebreakers

Tiebreakers between more than one teams in the Group rounds will apply in the following priorities

1. Points;
2. Average goal difference for the round;
3. Average number of goals scored;
4. If there are more than two teams in a tie, overall number of goals scored
including only games with the tied teams;
5. Penalty shootouts procedure among the tied teams.

### C. Automatic Penalty Shootouts Procedure 
* To  resolve  tie breaks  in  the  group  rounds  and  the  playoff  matches,  penalty  shootouts  will  be  used.  For penalty shootouts, we are going to use these parameters:
* *pen_dist_x: 42.5 (The ball will be placed 42.5m from the goal) 
* pen_allow_mult_kicks: true (allow multiple kicks so normal play) 
* pen_taken_wait: 200 (the number of cycles waited after start pen is 200)* 
(This means that the kicker starts 42.5 meters from the goal and can use multi turns and kicks. After at most 200 cycles, the shootout is stopped for the kicker).
We are going to use these parameters in the championship: 
* *nr_extra_halfs: 0 (no extra halves or matches)* 
(This means that teams cannot use heterogeneous players.)

## Code of Honor 

### A. Coach Messages 
The coach can issue arbitrary “freeform” messages except on play-on mode. The coach can send one advice, one info, and one definition, every 30 seconds – the rest will be ignored by the server. Therefore, the coach should not send more than three of those standard language directives per 30 seconds, so as not to flood the network.

### B. Fouls 
Free kicks and kick-ins are detected automatically by the soccer server in many relevant cases. Sometimes, however, fouls occur which can only be detected by the human referee who has to award a free kick to the disadvantaged team. Possible reasons to call a foul are:

* If one team surrounds the ball so that the other team cannot kick 
* If the goal is blocked by so many players so that the ball could not go in (rough guideline: a wall of players blocking the goal); 
* If a team intentionally blocks the movement of opponent players; 
* The number of goalie moves is limited to 2. It is possible to get around this by doing a small kick and catching again. This is allowed once then the referee is required to drop the ball on the closest corner of the penalty box (notice this practice is not encouraged we are just acknowledging the potential for miss - kicks 
– continual use may be considered violating the fair play commitment); 
* Anything else that appears to  violate  the  fair  play  commitment  may  also  be  called  as  a  foul  after consultation with the committee.

## Fair Play 

The goal of the game is play soccer according to fair and common sense understanding of soccer and to the  restrictions  imposed  by  the  virtual  simulated  world  of  the  soccer  server.  Circumvention  of  these restrictions is considered violating the fair play commitment and its use during the tournament games is strictly forbidden. Example: 

* Using another teams binaries in your team;
* Jamming the simulator by sending more than 3 or 4 commands per client per cycle;
* Using direct inter-process communication; 
* To  disturb  other  teams  communication  by  recording  and  sending  fake  communication  of  the opponent team. 

Any of the above is strictly forbidden
. 
Other strategies might be found violating the fair play commitment, 
after consultation among the committee. However, we expect it to be pretty clear what a fair team should 
l
ook like. In particular, the destructive disruption of opponent agent operation or the gain of advantage 
by other means than explicitly offered by the soccer server count as not fair play. If you are in doubt of 
using a certain method, please ask the commi
ttee before the tournament starts. If a team is found to use 
unfair programming methods during the tournament, it will be 
immediately disqualified
.
If a team is under suspicion of violating the fair
-
play agreement, the committee has the right to ask for 
source code inspection.